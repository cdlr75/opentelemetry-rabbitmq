import asyncio
import aio_pika

from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import ConsoleSpanExporter
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor
from opentelemetry.ext import jaeger

# ===========================================================================

trace.set_tracer_provider(TracerProvider())
# Log traces in the console
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(ConsoleSpanExporter())
)
# Export to jaeger
jaeger_exporter = jaeger.JaegerSpanExporter(
    service_name="consumer service", agent_host_name="localhost", agent_port=6831
)
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(jaeger_exporter)
)

# ===========================================================================


async def main(loop):
    connection = await aio_pika.connect_robust(
        "amqp://guest:guest@127.0.0.1/", loop=loop
    )

    queue_name = "test_queue"

    async with connection:
        # Creating channel
        channel = await connection.channel()

        # Declaring queue
        queue = await channel.declare_queue(queue_name, auto_delete=True)

        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    otheader = message.headers.get("Correlation-Context")
                    if otheader is None:
                        print("no telemetry header founded")
                        ctx = None
                    else:
                        ctx = trace.SpanContext(**{
                            'is_remote': True,
                            **{  # trace_id and value_id
                                key: int(value)
                                for param in otheader.split(",")
                                for key, value in [param.split("=")]
                            }
                        })
                        if not ctx.is_valid():
                            print("invalid telemetry context")
                            ctx = None

                    tracer = trace.get_tracer(__name__)
                    with tracer.start_as_current_span('message processing',
                                                      parent=ctx,
                                                      kind=trace.SpanKind.CONSUMER) as span:
                        print(message.body)
                        await asyncio.sleep(1)

                        if queue.name in message.body.decode():
                            break


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    loop.close()
