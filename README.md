Test Opentelemetry with rabbitmq
[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)
===

Here is simples *producer* and *consumer* implementations using aio-pika that emit opentelemetry traces.

Each *producer* request to the *consumer* create trace with a span from the *producer* and a span from the *consumer*.

The *producer* make a request every seconds and the *consumer* wait a second before responding.

Spans are logged into the console and exported toward Jaeger.

### Disclaimer

I am not familiar with Opentelemetry. Please see references links below.

## Quick start

Install requirements
```sh
pip install -r requirements.txt
```

Run Rabbitmq
```sh
docker run --rm -d --name some-rabbit --hostname my-rabbit -p 5672:5672 rabbitmq:3
```

Run Jaeger
```sh
docker run --rm --name jaeger -d -p 16686:16686 -p 6831:6831/udp jaegertracing/all-in-one
```

Start the consumer
```sh
python consumer.py
```

Run the producer
```sh
python producer.py
```

See the result on [Jaeger](http://localhost:16686).

![Jager result](result.png)

## References

- https://github.com/open-telemetry/opentelemetry-specification/blob/master/specification/trace/semantic_conventions/messaging.md#messaging-attributes
- https://opentelemetry-python.readthedocs.io/en/stable/getting-started.html
- https://opentelemetry-python.readthedocs.io/en/stable/api/trace.html
