import asyncio
import aio_pika

from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import ConsoleSpanExporter
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor
from opentelemetry.ext import jaeger

# ===========================================================================

trace.set_tracer_provider(TracerProvider())
# Log traces in the console
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(ConsoleSpanExporter())
)
# Export to jaeger
jaeger_exporter = jaeger.JaegerSpanExporter(
    service_name="producer service", agent_host_name="localhost", agent_port=6831
)
trace.get_tracer_provider().add_span_processor(
    SimpleExportSpanProcessor(jaeger_exporter)
)

# ===========================================================================


def get_otheader(span):
    return {
        "Correlation-Context": f"trace_id={span.context.trace_id},span_id={span.context.span_id}"
    }


async def main(loop):
    connection = await aio_pika.connect_robust(
        "amqp://guest:guest@127.0.0.1/", loop=loop
    )

    async with connection:
        routing_key = "test_queue"

        channel = await connection.channel()

        tracer = trace.get_tracer(__name__)
        with tracer.start_as_current_span('producer', kind=trace.SpanKind.PRODUCER, attributes={
            "messaging.system": "rabbitmq",
            "messaging.destination": "",
            "messaging.destination_kind": "queue",
            "messaging.rabbitmq.routing_key": routing_key,
        }) as span:

            await channel.default_exchange.publish(
                aio_pika.Message(
                    body="Hello {}".format(routing_key).encode(),
                    headers=get_otheader(span),
                ),
                routing_key=routing_key,
            )


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    loop.close()
